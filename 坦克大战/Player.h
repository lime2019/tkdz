#pragma once
#include"Tank.h"

// 设置玩家坦克出生点位置
const Pos_RC P1_born_pos = { 26,10 };

class Player:public Tank
{
private:
	int P1_tank_number;
public:
	Player(Pos_RC map_px = P1_born_pos, UnitType typ = P1, Direction dir = UP, Armor Lev = NORMAL);

	virtual float GetSpeed()const;
};

