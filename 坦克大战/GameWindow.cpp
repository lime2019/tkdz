#include "GameWindow.h"

GameWindow::GameWindow():p1_bullet_count(0)
{
}

void GameWindow::play()
{
	Unit* unit = &p1;//使用指针实现多态调用
	while (true)
	{
		//控制图像刷新时间
		static DWORD pic_timer = timeGetTime();
		DWORD now = timeGetTime();

		//如果按下ESC，退出游戏
		if (KEY_DOWN(Key_ESC))
		{
			break;
		}

		//如果到了刷新画面的时间，就执行绘制操作
		if (now - pic_timer >= RenewClock)
		{
			pic_timer = now;
			unit = &p1;
			if (unit)
			{
				if (unit->renewXYPos())
				{
					control(*unit, map);
				}
			}
			// 刷新子弹与页面
			refreshBullets();
			refreshWindow();
		}
		Sleep(RenewClock / 3);

	}
}

void GameWindow::refreshWindow()
{
	cleardevice();//清屏
	pictures.drawMap(map.GetAVal());//绘制地图
	if (!bullets.empty())//绘制炮弹
	{
		for (size_t i = 0; i < bullets.size(); i++)
		{
			pictures.drawBullet(bullets[i]);
		}
	}
	//绘制坦克，之后循环访问所有坦克
	pictures.drawTank(p1);
	pictures.drawJungle(map.GetAVal());//绘制丛林
	pictures.drawBooms();//绘制爆炸贴图
	//显示到屏幕
	FlushBatchDraw();
}

void GameWindow::refreshBullets()
{
	if (!bullets.empty())
	{
		for (auto it = bullets.begin(); it != bullets.end();)
		{
			if (it->renewXYPos())//如果补帧完成
			{
				//让子弹移动
				if (it->move(it->GetDirection(), map))//如果移动后有体积碰撞
				{
					if (it->GetOwner() == P1)
					{
						p1_bullet_count--;
					}
					//修改地形、增加爆炸点，并且删除这枚炮弹
					changeMap(*it);
					pictures.addBoomPoint(it->GetBoomXYPos());
					it = bullets.erase(it);
					continue;
				}
			}
			it++;
		}
	}
}

void GameWindow::control(Unit& unit, Map& map)
{
	UnitType type = unit.GetType();
	if (type == P1 || type == CP)//该条件测试用
	{
		//当有多个按键按下时，坦克不移动（按键锁定，只允许一个方向移动）
		bool key_state[DirectionCount] = { false };//保存当前按键状态
		int count = 0;//保存当前按下按键的个数
		//记录按键状态
		if (KEY_DOWN(Key_DOWN))
		{
			key_state[DOWN] = true;
		}
		if (KEY_DOWN(Key_UP))
		{
			key_state[UP] = true;
		}
		if (KEY_DOWN(Key_LEFT))
		{
			key_state[LEFT] = true;
		}
		if (KEY_DOWN(Key_RIGHT))
		{
			key_state[RIGHT] = true;
		}
		//统计有几个键按下
		for (size_t i = 0; i < DirectionCount; i++)
		{
			if (key_state[i])
			{
				count++;
			}
		}
		//如果只有一个键按下，移动坦克
		if (count == 1)
		{
			size_t i;
			for (i = 0; i < DirectionCount; i++)//找出被按下的键对应的方向
			{
				if (key_state[i] == true)
				{
					break;
				}
			}
			unit.move((Direction)i, map);//坦克移动
		}
		//控制炮弹发射
		if (KEY_DOWN(Key_SHOOT))
		{
			const int shoot_cd = 100;//射击CD
			static DWORD shoot_timer = timeGetTime() - shoot_cd;
			DWORD now = timeGetTime();
			if (now - shoot_timer >= shoot_cd)
			{
				shoot_timer = now;
				shoot(unit);
			}
		}
	}
}

void GameWindow::shoot(const Unit& tank)
{
	if (tank.GetType() == P1)
	{
		//玩家坦克，装甲等级低于LIGHT的时候，只能发射一枚炮弹
		if (tank.GetArmorLev() <= LIGHT)
		{
			if (p1_bullet_count >= normal_player_bullets)
			{
				//如果当前发射的炮弹数目已达上限，直接跳出
				return;
			}
		}
		else//更高装甲等级可以发射两枚炮弹
		{
			if (p1_bullet_count == max_player_bullets)
			{
				//如果当前发射的炮弹数目已达上限，直接跳出
				return;
			}
		}
		p1_bullet_count++;//记录炮弹数目
	}

	bullets.push_back(Bullet(tank));//创建一个子弹到容器中
}

void GameWindow::changeMap(const Bullet& bullet)
{
	DestroyLev dLev = NoDestroy;
	unsigned int owner = bullet.GetOwner();
	Armor armorLev = bullet.GetArmorLev();
	Direction dir = bullet.GetDirection();
	const Pos_RC(*check_points_pos)[MapIndexCount] = bullet.GetCheckPointsPos();
	const MapInt(*check_points_val)[MapIndexCount] = bullet.GetCheckPointsVal();
	const bool(*touch_flags)[LayerCount][MapIndexCount] = bullet.GetTouchFlags();
	//如果碰撞点是边界，就直接退出这个函数
	if (map.GetVal((*check_points_pos)[0]) == BORDER)
	{
		return;
	}
	//根据坦克的阵营和火力等级，确定毁灭地形的程度
	if (owner == CP || owner == P1 && armorLev <= STRONG)
	{
		//如果坦克是敌军（或者玩家的火力较低），设置一次可以消除半块砖
		dLev = HalfDestroy;
	}
	else if (owner == P1 && armorLev == HEAVY)
	{
		dLev = AllDestroy;
	}
	//优化后的碰撞处理
	bool flag_1 = false;//标记第一层砖是否已经发生碰撞
	for (size_t iLayer = 0; iLayer < 2 && !flag_1; iLayer++)//用来区分砖块的两层
	{
		for (size_t iMap = 0; iMap < 2; iMap++)//用来区分两块砖
		{
			if ((*check_points_val)[iMap] > EMPTY && (*check_points_val)[iMap] <= BORDER)
			{
				if ((*touch_flags)[iLayer][iMap] == true)
				{
					flag_1 = true;//如果第一层就已经发生了碰撞，不会检查第二层砖
					map.DestroyMap((*check_points_pos)[iMap], dir, dLev);
				}
			}
		}
	}
}