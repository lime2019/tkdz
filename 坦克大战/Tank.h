#pragma once
#include"Unit.h"

// 坦克类，向下派生玩家坦克和电脑坦克
class Tank:public Unit
{
private:
	bool trackState;//切换履带显示
	DWORD timer_trackState;//记录履带切换时的时间点

public:
	Tank(Pos_RC map_pos, UnitType type, Direction dir = DOWN, Armor Lev = NORMAL);

	//获取履带状态
	bool GetTrackState()const;

	//坦克移动
	virtual bool move(Direction dir, const Map& map);

	//切换履带显示
	void renewTrackState();
};

