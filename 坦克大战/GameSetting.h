#pragma once
#include <math.h>
#include <graphics.h>

// 引用Winmm.lib从而可以使用Windows操作多媒体文件
#pragma comment(lib,"Winmm.lib")

// 宏定义，提高性能
#define Int unsigned short
#define MapInt unsigned char

//按键监测，参数为键盘的键值，当某按键被按下时返回非0值
#define KEY_DOWN(VK_NONAME) (GetAsyncKeyState(VK_NONAME) & 0x8000)	

//绘图坐标
typedef struct
{
	float x, y;
}Pos_XY;


// 原地图单元对应的像素大小
const Int source_map_px = 8;
// 每个单位（坦克、道具等）原始素材的像素大小
const Int source_unit_px = source_map_px * 2;
// 图片像素放大倍数，放大界面
const Int px_multiple = 3;

// 每个地图单元的实际像素大小
const Int map_px = px_multiple * source_map_px;

// 爆炸贴图的像素宽度
const Int source_boom_px = source_unit_px * 2;

// 坦克、道具的绘图大小
const Int unit_px = map_px * 2;

// 实际爆炸贴图大小
const Int boom_px = unit_px * 2;

// 子弹原始素材的像素大小
const Int source_half_map_px = source_map_px / 2;

// 子弹绘图大小
const Int half_map_px = map_px / 2;


//地图数组的行、列数
const Int map_row = 30;
const Int map_col = 32;

//单元数组的行、列数，为地图数组的两倍，以此达到坦克等移动“半格”的效果
const Int unit_row = map_row * 2;
const Int unit_col = map_col * 2;

//游戏界面的宽和高（像素）
const Int map_wide = map_px * map_col;
const Int map_height = map_px * map_row;

//每个单元在unit矩阵中占用的宽度（坦克长宽为4，炮弹为2）
const Int unit_size = 4;
const Int bullet_size = unit_size / 2;
const Int unit_sizeInMap = unit_size / 2;


// 页面刷新率FPS
const Int FPS = 60;
//画面刷新周期
const Int RenewClock = 1000 / FPS;

//每次刷新时物体应移动的像素数*1000（不能用整数直接代表像素，否则画面放大倍数会影响实际移动速度）
enum  Speed
{
	//每秒走2大格
	SlowSpeed = (2 * unit_sizeInMap * map_px) * RenewClock, 
	//每秒走3.5大格
	NormalSpeed = (int)(3.5 * unit_sizeInMap * map_px) * RenewClock, 
	//每秒走5大格
	FastSpeed = (5 * unit_sizeInMap * map_px) * RenewClock,

	HighSpeed = (int)(1.25 * FastSpeed),
	VeryHighSpeed = (int)(2 * HighSpeed)
};


//地图类型
enum MapType
{
	EMPTY,//空地
	WALL = 0x0F,//砖墙
	IRON,//防爆门
	BORDER,//地图边界
	SEA,//海面
	ICE = 0x21,//冰面
	JUNGLE,//丛林
};

//各单位类型值，用于碰撞检测
enum UnitType
{
	//子弹
	BULLET = 500,
	P1 = 300,//1号玩家
	P2 = 400,//2号玩家
	CP = 600,//敌人
	COMMANDER = 0xC8,//指挥官
};

//方向，绘制、开炮时用
enum Direction
{
	UP, LEFT, DOWN, RIGHT, DirectionCount
};

//坦克装甲等级
enum Armor
{
	DEAD = -1, NORMAL, LIGHT, STRONG, HEAVY, ArmorCount
};

//键盘控制
enum Key
{
	Key_UP = 'W', Key_LEFT = 'A', Key_RIGHT = 'D', Key_DOWN = 'S',
	Key_SHOOT = 'J', Key_PAUSE = 'P',
	Key_START = 'F', Key_SELECT = 'R',
	Key_ESC = VK_ESCAPE
};

// 读取文件
bool ExtractResource(LPCTSTR strDstFile, LPCTSTR strResType, LPCTSTR strResName);