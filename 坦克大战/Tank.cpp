#include "Tank.h"

// 构造函数
Tank::Tank(Pos_RC map_pos, UnitType type, Direction dir, Armor Lev): Unit(map_pos, type, dir), trackState(false)
{
	timer_trackState = timeGetTime();
	SetArmorLev(Lev);
}

// 获取坦克履带状态，主要是用来判断加载不同履带形状的坦克图片
bool Tank::GetTrackState() const
{
	return trackState;
}


// 进行坦克移动：主要是调用Unit中的move方法
//     参数1：坦克的运动方向
//     参数2：对应的Map对象
bool Tank::move(Direction dir, const Map& map)
{
	renewTrackState();//控制履带切换
	return Unit::move(dir, map);
}


void Tank::renewTrackState()
{
	DWORD renewClock = (DWORD)(px_multiple / (GetSpeed() / RenewClock)); //求出履带切换周期
	DWORD now = timeGetTime();//调用此函数时的时间
	//如果到了转换周期，就切换履带显示
	if (now - timer_trackState >= renewClock)
	{
		timer_trackState = now;
		trackState = !trackState;
	}
}