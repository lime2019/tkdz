#pragma once
#include"GameSetting.h"
#include"GameImge.h"
#include"Player.h"
#include"Map.h"
#include"Bullet.h"

#include<vector>

using std::vector;

const unsigned int max_player_bullets = 6;//玩家同时发射的子弹数量上限
const unsigned int normal_player_bullets = 3;

class GameWindow
{
private:
	GameImge pictures;//图形对象，管理所有的图片绘制
	Map map;//地图
	Player p1;
	vector<Bullet> bullets;//保存所有的子弹
	unsigned int p1_bullet_count;//记录玩家一已经发射的炮弹数

public:
	GameWindow();
	//开始游戏
	void play();

protected:
	//刷新画面
	void refreshWindow();
	//刷新子弹状态
	void refreshBullets();
	//控制函数，传入其它可移动单位，也能让玩家进行控制
	void control(Unit& unit, Map& map);
	//射击
	void shoot(const Unit& tank);
	//改变地形
	void changeMap(const Bullet& bullet);

};

