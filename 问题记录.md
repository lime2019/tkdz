## 1. Git问题

#### 1.1：码云网页中显示图片问题

- 问题gif图：

  ![](./errorImage/gitError1.gif)

- 原因：

  路径中有中文符

- 解决方法：

  将文件夹重命名为英文字符

#### 1.2：冲突问题

- 问题图片：

  ![](./errorImage/gitError2.png)

- 原因：

  我在网页上直接修改了文件，然后本地相同文件我也做了修改，造成拉取时出现冲突

- 解决方法：

  ![](./errorImage/gitError2_1.png)

  将其中冲突部分修改掉，重新提交即可！

  其中`<<<<<<HEAD`是你写的代码；

  `>>>>>>>`+一串字符串是别人写的代码；

## 2. 贴图问题：

#### 2.1 ：图片黑边问题

- 问题gif图

  ![](./errorImage/drawError1.gif)

- 原因：

  使用的坦克图片周围是透明的，所以我们设置的背景颜色对其没有影响-

- 解决方法：

  EaxyX官方有给解决方法：[详解透明贴图和三元光栅操作](https://codeabc.cn/yangw/a/transparent-putimage)

  我们使用的是第三种，最简单的解决方法：

  ![](./errorImage/drawError1_1.png)

  引用图片的原始代码：

  ```c++
  putimage(pos.x, pos.y, &img[GetArmorLev()][GetDirection()][0]);
  ```

  将原来的代码改为：

  ```C++
  putimage(pos.x, pos.y, &img_hide[GetArmorLev()][GetDirection()], SRCAND);
  putimage(pos.x, pos.y, &img[GetArmorLev()][GetDirection()][0], SRCPAINT);
  ```

  SRCAND 表示“掩码图 AND 目标图”；SRCPAINT 表示“源图 OR 目标图”；

  先加载遮罩图，得到黑色的待加载区；然后再加载图片素材，实现透明贴图的效果；

#### 2.2：图片重叠问题

- 问题gif图：

  ![](./errorImage/drawError2.gif)

- 原因：原来坦克图片未擦除，形成叠影

- 解决方法：使用背景色擦除屏幕上，之前的坦克图片

  原来代码：

  ```c++
  setbkcolor(BLUE);//设置背景色
  cleardevice();//使用背景色清空屏幕
  while (true)
  {
      Pos_XY pos = player1.GetPos();
      if (KEY_DOWN(Key_DOWN)) {
          pos.y++;
          player1.SetPos(pos);
      }
      player1.show();
      Sleep(10);
  }
  ```

  修改后代码：

  ```c++
  setbkcolor(BLUE);//设置背景色
  cleardevice();//使用背景色清空屏幕
  while (true)
  {
      Pos_XY pos = player1.GetPos();
      if (KEY_DOWN(Key_DOWN)) {
          pos.y++;
          player1.SetPos(pos);
      }
      cleardevice();//使用背景色清空屏幕
      player1.show();
      Sleep(10);
  }
  ```

#### 2.3：图片闪烁问题

- 问题gif图：

  ![](./errorImage/drawError3.gif)

- 原因：硬件访问带来的问题

- 解决方法：

  使用EasyX库中的BeginBatchDraw函数

  > 这个函数用于开始批量绘图。执行后，任何绘图操作都将暂时不输出到绘图窗口上，直到执行 FlushBatchDraw 或 EndBatchDraw  才将之前的绘图输出。
  >
  > 通过批量绘图功能，可以消除闪烁





## 2. 地图

砖块取：0f

边界取：11

钢块取：10

湖泊：12

丛林：22

冰面：21

空地：00

地图大小（坦克活动范围）为26行×26行

整体为：30行×32列